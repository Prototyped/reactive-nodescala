package nodescala



import scala.language.postfixOps
import scala.util.{Try, Success, Failure}
import scala.collection._
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.async.Async.{async, await}
import org.scalatest._
import NodeScala._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class NodeScalaSuite extends FunSuite {

  test("A Future should always be created") {
    val always = Future.always(517)

    assert(Await.result(always, 0 nanos) == 517)
  }

  test("A Future should never be created") {
    val never = Future.never[Int]

    try {
      Await.result(never, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }
  }

  test("any() returns a future satisfied when any of the futures in the list are satisfied") {
    val waiter = List(Future.delay(500 millis) collect { case _ => 0 },
                      Future.delay(200 millis) collect { case _ => 1 },
                      Future.delay(400 millis) collect { case _ => 2 })
    val anyResult = Future.any(waiter)
    try {
      Await.result(anyResult, 100 millis)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }

    // 100 ms in
    assert(Await.result(anyResult, 450 millis) === 1)
  }

  test("all() returns a future satisfied when all of the futures in the list are satisfied") {
    val waiter = List(Future.delay(500 millis) collect { case _ => 0 },
                      Future.delay(300 millis) collect { case _ => 1 },
                      Future.delay(200 millis) collect { case _ => 2 })
    val anyResult = Future.all(waiter)
    try {
      Await.result(anyResult, 100 millis)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }

    // 100 ms in
    try {
      Await.result(anyResult, 250 millis)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }

    // 350 ms in
    assert(Await.result(anyResult, 200 millis) === List(0, 1, 2))
  }

  test("delay() with a finite delay returns a future satisfied after the delay is over") {
    val waiter = Future.delay(200 millis)
    try {
      Await.result(waiter, 100 millis)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }

    // 100 ms in
    assert(Await.result(waiter, 150 millis) === ())
  }

  test("delay() with an infinite delay never returns") {
    val waiter = Future.delay(Duration.Inf)
    try {
      Await.result(waiter, 1 second)
      assert(false)
    } catch {
      case t: TimeoutException => // ok!
    }
  }

  test("now() returns the value if the Future is complete") {
    val waiter = Future.delay(200 millis) collect { case _ => 42 }
    try {
      blocking {
        Thread.sleep((100 millis).toMillis)
      }
      waiter.now
      assert(false)
    } catch {
      case t: NoSuchElementException => // ok!
    }

    // 100 ms in
    Await.result(waiter, 150 millis)
    assert(waiter.now === 42)
  }

  test("continueWith() performs a deferred transform") {
    val waiter = Future.delay(200 millis) collect { case _ => 7 }
    val continued = waiter continueWith { f: Future[Int] =>
      Await.result(f, Duration.Zero) * 6
    }

    assert(Await.result(continued, 250 millis) === 42)

    val failingWaiter = Future.delay(200 millis) collect ({
      case _ => throw new IllegalStateException
    } : PartialFunction[Unit, Int])
    val hopefulContinued = failingWaiter continueWith { f: Future[Int] =>
      Await.result(f, Duration.Zero) * 6
    }

    try {
      Await.result(hopefulContinued, 250 millis)
      assert(false)
    } catch {
      case t: IllegalStateException => // ok!
    }

    val succeedingWaiter = Future.delay(200 millis) collect { case _ => 7 }
    val failingContinued = failingWaiter continueWith { _ => throw new IllegalStateException }
    try {
      Await.result(failingContinued, 250 millis)
      assert(false)
    } catch {
      case t: IllegalStateException => // ok!
    }
  }

  test("continue() performs a deferred transform") {
    val waiter = Future.delay(200 millis) collect { case _ => 7 }
    val continued = waiter continue {
      case Success(value) => value * 6
      case Failure(exception) => throw exception
    }

    assert(Await.result(continued, 250 millis) === 42)

    val failingWaiter = Future.delay(200 millis) collect ({
      case _ => throw new IllegalStateException
    } : PartialFunction[Unit, Int])
    val hopefulContinued = failingWaiter continue {
      case Success(value) => value * 6
      case Failure(exception) => throw exception
    }

    try {
      Await.result(hopefulContinued, 250 millis)
      assert(false)
    } catch {
      case t: IllegalStateException => // ok!
    }

    val succeedingWaiter = Future.delay(200 millis) collect { case _ => 7 }
    val failingContinued = succeedingWaiter continue {
      case Success(value) => throw new IllegalStateException
      case Failure(exception) => throw new IllegalArgumentException(exception)
    }

    try {
      Await.result(failingContinued, 250 millis)
      assert(false)
    } catch {
      case t: IllegalStateException => // ok!
    }
  }

  test("CancellationTokenSource should allow stopping the computation") {
    val cts = CancellationTokenSource()
    val ct = cts.cancellationToken
    val p = Promise[String]()

    async {
      while (ct.nonCancelled) {
        // do work
      }

      p.success("done")
    }

    cts.unsubscribe()
    assert(Await.result(p.future, 1 second) == "done")
  }

  test("run() kicks off computation that can be cancelled via unsubscription") {
    val start = System.currentTimeMillis()
    val waiter = Future.run() { token: CancellationToken =>
      Future {
        while (token.nonCancelled) {
          blocking {
            Thread.sleep((100 millis).toMillis)
          }
        }
      }
    }

    val unsubscription = Future.delay(500 millis)
    unsubscription onSuccess {
      case _ => waiter.unsubscribe()
    }

    Await.result(unsubscription, 550 millis)
    assert(System.currentTimeMillis() - start >= (500 millis).toMillis)
  }

  class DummyExchange(val request: Request) extends Exchange {
    @volatile var response = ""
    val loaded = Promise[String]()
    def write(s: String) {
      response += s
    }
    def close() {
      loaded.success(response)
    }
  }

  class DummyListener(val port: Int, val relativePath: String) extends NodeScala.Listener {
    self =>

    @volatile private var started = false
    var handler: Exchange => Unit = null

    def createContext(h: Exchange => Unit) = this.synchronized {
      assert(started, "is server started?")
      handler = h
    }

    def removeContext() = this.synchronized {
      assert(started, "is server started?")
      handler = null
    }

    def start() = self.synchronized {
      started = true
      new Subscription {
        def unsubscribe() = self.synchronized {
          started = false
        }
      }
    }

    def emit(req: Request) = {
      val exchange = new DummyExchange(req)
      if (handler != null) handler(exchange)
      exchange
    }
  }

  class DummyServer(val port: Int) extends NodeScala {
    self =>
    val listeners = mutable.Map[String, DummyListener]()

    def createListener(relativePath: String) = {
      val l = new DummyListener(port, relativePath)
      listeners(relativePath) = l
      l
    }

    def emit(relativePath: String, req: Request) = this.synchronized {
      val l = listeners(relativePath)
      l.emit(req)
    }
  }

  test("Listener should serve the next request as a future") {
    val dummy = new DummyListener(8191, "/test")
    val subscription = dummy.start()

    def test(req: Request) {
      val f = dummy.nextRequest()
      dummy.emit(req)
      val (reqReturned, xchg) = Await.result(f, 1 second)

      assert(reqReturned == req)
    }

    test(immutable.Map("StrangeHeader" -> List("StrangeValue1")))
    test(immutable.Map("StrangeHeader" -> List("StrangeValue2")))

    subscription.unsubscribe()
  }

  test("Server should serve requests") {
    val dummy = new DummyServer(8191)
    val dummySubscription = dummy.start("/testDir") {
      request => for (kv <- request.iterator) yield (kv + "\n").toString
    }

    // wait until server is really installed
    Thread.sleep(500)

    def test(req: Request) {
      val webpage = dummy.emit("/testDir", req)
      val content = Await.result(webpage.loaded.future, 1 second)
      val expected = (for (kv <- req.iterator) yield (kv + "\n").toString).mkString
      assert(content == expected, s"'$content' vs. '$expected'")
    }

    test(immutable.Map("StrangeRequest" -> List("Does it work?")))
    test(immutable.Map("StrangeRequest" -> List("It works!")))
    test(immutable.Map("WorksForThree" -> List("Always works. Trust me.")))

    dummySubscription.unsubscribe()
  }

}




