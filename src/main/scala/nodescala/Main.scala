package nodescala

import scala.language.postfixOps
import scala.concurrent._
import scala.concurrent.duration._
import ExecutionContext.Implicits.global
import scala.async.Async.{async, await}
import scala.util.{Success, Failure, Try}

object Main {

  def main(args: Array[String]) {
    // TO IMPLEMENT
    // 1. instantiate the server at 8191, relative path "/test",
    //    and have the response return headers of the request
    val myServer = new NodeScala.Default(8191)
    val myServerSubscription = myServer.start("/test") { request: NodeScala.Request =>
      for (kv <- request.iterator)
        yield (kv + "\n").toString
    }

    // TO IMPLEMENT
    // 2. create a future that expects some user input `x`
    //    and continues with a `"You entered... " + x` message
    val userInterrupted: Future[String] = {
      val p = Promise[String]()
      val input = Future.userInput("Type something >\n")
      input onComplete { tried: Try[String] =>
        tried match {
          case Success(value) =>
            println(s"You entered... $value")
            p complete `tried`
          case Failure(exception) =>
            p complete `tried`
        }
      }
      p.future
    }

    // TO IMPLEMENT
    // 3. create a future that completes after 20 seconds
    //    and continues with a `"Server timeout!"` message
    val timeOut: Future[String] = {
      Future {
        blocking {
          Thread.sleep((20 seconds).toMillis)
        }
        "Server timeout!"
      }
    }

    // TO IMPLEMENT
    // 4. create a future that completes when either 10 seconds elapse
    //    or the user enters some text and presses ENTER
    val terminationRequested: Future[String] = {
      val p = Promise[String]()
      p tryCompleteWith timeOut
      p tryCompleteWith userInterrupted
      p.future
    }

    // TO IMPLEMENT
    // 5. unsubscribe from the server
    terminationRequested onSuccess {
      case msg => {
        println(msg)
        myServerSubscription.unsubscribe()
        println("Bye!")
      }
    }
  }

}
